// JavaScript Document
var firstapp = angular.module('firstapp', ['ngRoute', 'phonecatControllers', 'templateservicemod', 'navigationservice', 'angularFileUpload', 'textAngular', 'ui.sortable', 'angular-loading-bar']);

/*
Client ID : 951565546728-s8mpt0u6nblg7qv6mpom6arvqm2o7qp8.apps.googleusercontent.com
Client Secret ID: qVyozubT7R6e2R3opkQq1Rhv
*/

firstapp.config(['$routeProvider',
  function ($routeProvider) {
        $routeProvider.
        when('/home', {
            templateUrl: 'views/template.html',
            controller: 'homeCtrl'
        }).
        when('/tournaments', {
            templateUrl: 'views/template.html',
            controller: 'tournamentsCtrl'
        }).
        when('/categories/:tournamentid', {
            templateUrl: 'views/template.html',
            controller: 'categoriesCtrl'
        }).
        when('/login', {
            templateUrl: 'views/login.html',
            controller: 'loginCtrl'
        }).
        when('/createfixtures/:editid', {
            templateUrl: 'views/template.html',
            controller: 'createfixturesCtrl'
        }).
        when('/createteam/:editid', {
            templateUrl: 'views/template.html',
            controller: 'createteamCtrl'
        }).
        when('/fixtures', {
            templateUrl: 'views/template.html',
            controller: 'fixturesCtrl'
        }).
        when('/gallery', {
            templateUrl: 'views/template.html',
            controller: 'galleryCtrl'
        }).
        when('/groups/:categoryid', {
            templateUrl: 'views/template.html',
            controller: 'groupsCtrl'
        }).
        when('/photoupload', {
            templateUrl: 'views/template.html',
            controller: 'photouploadCtrl'
        }).
        when('/teams', {
            templateUrl: 'views/template.html',
            controller: 'teamsCtrl'
        }).
        when('/registrations', {
            templateUrl: 'views/template.html',
            controller: 'registrationsCtrl'
        }).
        when('/notifications', {
            templateUrl: 'views/template.html',
            controller: 'notificationsCtrl'
        }).
        when('/createnotification', {
            templateUrl: 'views/template.html',
            controller: 'createNotificationCtrl'
        }).

        when('/addteam/:groupname/:groupid', {
            templateUrl: 'views/template.html',
            controller: 'addTeamCtrl'
        }).
				when('/pointtablegenerator', {
            templateUrl: 'views/template.html',
            controller: 'pointtablegeneratorCtrl'
        }).

        otherwise({
            redirectTo: $.jStorage.get('user') ? '/tournaments' : '/login'
        });
  }
]);

firstapp.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});



firstapp.filter('getproperty', function () {
    return function (value, key) {
        key = 'name';
        return Object.getOwnPropertyDescriptor(value, key).writable;
    }
})

firstapp.filter('getidproperty', function () {
    return function (value, key) {
        key = 'id';
        return Object.getOwnPropertyDescriptor(value, key).writable;
    }
})

firstapp.filter('getscoreproperty', function () {
    return function (value, key) {
        key = 'score';
        return Object.getOwnPropertyDescriptor(value, key).writable;
    }
})

firstapp.filter('setproperty', function () {
    return function (value, key) {
        return Object.defineProperty(value, key, {
            writable: true
        });
    }
});

firstapp.filter('setpropertyfalse', function () {
    return function (value, key) {
        console.log(key);
        return Object.defineProperty(value, key, {
            writable: false
        });
    }
});

firstapp.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelsetter = model.assign;
            var onChange = $parse(attrs.onChange);
            element.bind('change', function () {
                scope.$apply(function () {
                    modelsetter(scope, element[0].files[0]);
                    console.log(attrs);
                    onChange(scope);
                });
            });
        }

    };
}]);

firstapp.directive(
    'dateInput',
    function (dateFilter) {
        console.log(dateFilter);
        return {
            require: 'ngModel',
            template: '<input type="date"></input>',
            replace: true,
            link: function (scope, elm, attrs, ngModelCtrl) {
                console.log(ngModelCtrl);
                ngModelCtrl.$formatters.unshift(function (modelValue) {
                    console.log(modelValue);
                    return dateFilter(modelValue, 'yyyy-MM-dd');
                });

                ngModelCtrl.$parsers.unshift(function (viewValue) {
                    console.log(modelValue);
                    return new Date(viewValue);
                });
            },
        };
    });
// firstapp.filter('filterwithgroupid', function() {
//   function checkforid(group) {
//     return group.id == this.key;
//   }
//   return function(arraytobefiltered, arraytoserach, key) {
//     var groups = arraytoserach.filter(checkforid, key);
//
//   };
// });
firstapp.filter('imagepath', function () {
    return function (input) {
        //return "http://localhost/rest/rest/uploads/" + input;
        return "http://pixoloproductions.com/fcb/fcbescolarest/uploads/" + input;
    };
});

firstapp.filter('strReplace', function () {
    return function (input) {
        input = input || '';
        return input.replace(new RegExp('$$', 'g'), '');
    };
});
