var phonecatControllers = angular.module('phonecatControllers', ['templateservicemod', 'navigationservice']);

// //var adminurl = "http://localhost/rest/rest/index.php/";
// //var imageurl = "http://localhost/rest/rest/uploads/";
// var adminurl = "http://learnwithinq.com/adminpanel/rest/index.php/";
var imageurl = "http://www.pixoloproductions.com/fcb/fcbescolarest/uploads/";

var userarray = [{
    'image': 'admin.png',
    'post': 'Administrator'
}];

phonecatControllers.controller('home', ['$scope', 'TemplateService', 'NavigationService', '$rootScope', '$filter', '$window', '$location',
  function ($scope, TemplateService, NavigationService, $rootScope, $filter, $window, $location) {
        $scope.template = TemplateService;
        TemplateService.content = "views/content.html";
        $scope.title = "dashboard";
        $scope.navigation = NavigationService.getnav();


  }
]);

phonecatControllers.controller('loginCtrl', ['$scope', 'TemplateService', 'NavigationService', '$rootScope', '$location',
  function ($scope, TemplateService, NavigationService, $rootScope, $location) {
        $scope.template = TemplateService;
        TemplateService.content = "views/login.html";
        $scope.title = "dashboard";
        // Initializations
        $scope.adminpaneluser = {};

        // callbacks
        loginsuccess = function (response) {
            //            console.log(response);
            if (response.data) {
                var userdata = {};
                userdata.id = response.data.id;
                userdata.name = response.data.name;
                userdata.email = response.data.email;
                userdata.access = response.data.access;
                $.jStorage.set('user', userdata);
                $location.path('tournaments');
            } else {
                $scope.errormessage = "Email ID and password didn't match !";
            }
        }

        loginerror = function (error) {
            //            console.log('Internet Error');

        }

        // Scope FUNCTIONS
        $scope.login = function () {
            console.log('called');
            NavigationService.login($scope.adminpaneluser).then(loginsuccess, loginerror);

        }


  }
]);

phonecatControllers.controller('tournamentsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location',
  function ($scope, TemplateService, NavigationService, $location) {
        $scope.template = TemplateService;
        TemplateService.content = "views/tournaments.html";
        $scope.title = "tournamentsCtrl";

        // Initializations
        $scope.tournaments = [];
        $scope.newtournament = {};
        $scope.hidetextfields = true;
        var originaldata = {};

        var index;

        // CallBacks


        getalldatasuccess = function (response) {
            //            console.log(response);
            $scope.tournaments = _.map(response.data, function (element) {
                Object.defineProperty(element, 'name', {
                    writable: false
                });
                return element;
            });
        }
        getalldataerror = function (error) {
            //            console.log('Internet Error');
        }
        editdatasuccess = function (response) {
            //            console.log(response);
        }
        editdataerror = function (error) {
            //            console.log('Internet Error');
        }

        deletedatabyidsuccess = function (response) {

            //            console.log(response);
        };
        deletedatabyiderror = function (error) {
            //            console.log('Internet Error');
        };

        // Scopes


        $scope.savetournament = function (newtournament) {
            if (newtournament.name) {
                if (newtournament.id) {
                    NavigationService.editdata('tournaments', {
                        "id": newtournament.id,
                        "data": angular.toJson(newtournament)
                    }).then(editdatasuccess, editdataerror);
                    newtournament.notdisabled = true;
                    Object.defineProperty(newtournament, 'name', {
                        writable: false
                    });
                } else {
                    NavigationService.storepostdata('tournaments', newtournament).then(function (response) {
                        //                        console.log(response);
                        newtournament.id = response.data;
                        Object.defineProperty(newtournament, 'name', {
                            writable: false
                        });
                    }, function (error) {

                    });

                }
            }
        };


        $scope.deletetournament = function (tournament) {

            NavigationService.deletedatabyid('tournaments', tournament.id).then(function (response) {
                //                console.log(response);
                $scope.tournaments.splice($scope.tournaments.indexOf(tournament), 1);
            }, function (error) {

            });
        };

        $scope.addnewtournament = function () {
            $scope.tournaments.push({
                name: ''
            });
            Object.defineProperty($scope.tournaments[$scope.tournaments.length - 1], 'name', {
                writable: true
            });
        }

        $scope.canceladdition = function (tournament) {
            if (tournament.id) {
                //                console.log(originaldata[tournament.id]);
                tournament.name = originaldata[tournament.id].name;
                tournament.primary_color = originaldata[tournament.id].primary_color;
                tournament.secondary_color = originaldata[tournament.id].secondary_color;



                //                console.log($scope.tournaments[$scope.tournaments.indexOf(tournament)]);
                Object.defineProperty(tournament, 'name', {
                    writable: false
                });
            } else {
                var index = $scope.tournaments.indexOf(tournament);
                $scope.tournaments.splice(index, 1);
            }

        }
        $scope.edittournament = function (tournament) {
            originaldata[tournament.id] = angular.copy(tournament);
            Object.defineProperty(tournament, 'name', {
                writable: true
            });
            //            console.log(tournament);

        }

        // Navigation services

        NavigationService.getalldata('tournaments').then(getalldatasuccess, getalldataerror);

        $scope.gotocategorypage = function (tournamentid) {
            $.jStorage.set('tournament', tournamentid)
            $location.path('categories/' + tournamentid);
        }
  }
]);

phonecatControllers.controller('categoriesCtrl', ['$scope', 'TemplateService', 'NavigationService', '$routeParams', '$location',
  function ($scope, TemplateService, NavigationService, $routeParams, $location) {
        $scope.template = TemplateService;
        TemplateService.content = "views/categories.html";
        $scope.title = "categories";

        // Initializations
        var tournamentid = $routeParams.tournamentid;
        $scope.categories = [];
        var originaldata = {};

        // Callbacks Functions
        getmanybysuccess = function (response) {
            //            console.log(response);
            $scope.categories = _.map(response.data, function (element) {
                Object.defineProperty(element, 'name', {
                    writable: false
                });
                return element;
            });
        }
        getmanybyerror = function (error) {
            //            console.log('Internet Error');
        }


        // Scope functions

        $scope.deletecategory = function (category) {
            NavigationService.deletedatabyid('categories', category.id).then(function (response) {
                $scope.categories.splice($scope.categories.indexOf(category), 1);
            }, function (error) {

            });
        };

        $scope.addcategory = function () {
            $scope.categories.push({});
        }

        $scope.savecategory = function (category) {
            if (category.name) {
                if (category.id) {
                    NavigationService.editdata('categories', {
                        "id": category.id,
                        "data": angular.toJson(category)
                    }).then(function (response) {
                        Object.defineProperty(category, 'name', {
                            writable: false
                        });
                    }, function (error) {});
                } else {
                    category.tournament_id = tournamentid;
                    NavigationService.storepostdata('categories', category).then(function (response) {
                        category.id = response.data;
                        Object.defineProperty(category, 'name', {
                            writable: false
                        });
                    }, function (error) {});

                }
            }
        };
        $scope.editcategory = function (category) {
            originaldata[category.id] = angular.copy(category);
            Object.defineProperty(category, 'name', {
                writable: true
            });

        }

        $scope.cancelcategorycreation = function (category) {
            if (category.id) {
                category.name = originaldata[category.id].name;
                category.age = originaldata[category.id].age;
                Object.defineProperty(category, 'name', {
                    writable: false
                });

            } else {
                var index = $scope.categories.indexOf(category);
                $scope.categories.splice(index, 1);
            }

        }

        // Age validations
        $scope.validateage = function (category) {
            var pattern = /^[a-zA-Z!@#\$%\^\&*\)\(+=._-]+$/gi;
            var char = category.age.substr(category.age.length - 1);
            if (char.match(pattern)) {
                $scope.errormessage = "Age cannot include alphabets and special characters";
                category.age = category.age.substr(0, category.age.length - 1);
            } else if (parseInt(category.age) > 45) {
                $scope.errormessage = "Age cannot exceed to 45";
                category.age = category.age.substr(0, category.age.length - 1);
            }
        }

        $scope.gotogroupspage = function (categoryid) {
            $location.path('groups/' + categoryid);
        }

        // Navigation services

        NavigationService.getmanyby('categories', 'tournament_id', tournamentid).then(getmanybysuccess, getmanybyerror)


  }
]);

phonecatControllers.controller('createfixturesCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', 'FileUploader', '$rootScope', '$routeParams', 'dateFilter', '$filter',
  function ($scope, TemplateService, NavigationService, $location, FileUploader, $rootScope, $routeParams, dateFilter, $filter) {
        $scope.template = TemplateService;
        TemplateService.content = "views/createfixtures.html";
        $scope.title = "createfixtures";

        var tournamentid = $routeParams.editid;
        $scope.category = [];
        //  $scope.category.index = 0;
        //  $scope.category = [];
        //  Initializations
        $scope.fixtures = [];
        $scope.fixturesinfo = [];
        $scope.matchtypes = [];
        $scope.venues = [];
        $scope.teamofcategory = {};
        var originaldata = {};
        var checkforindex = function (group) {
            //            console.log(group.id);
            index = group.teams.findIndex((team) => team.id == parseInt(this));
            return index != -1;
        };

        var getindex = function (category) {
            //            console.log(parseInt(this));
            index = category.groups.findIndex(checkforindex, parseInt(this));
            return index != -1;
        };

        // Add Category indexes
        var addcategoryindex = function () {

            if ($scope.category.length == 0 && $scope.fixturesinfo.length > 0) {
                //                console.log('add category index');
                _.forEach($scope.fixtures, function (value, key) {
                    value.date = new Date(value.date);
                    $scope.category[key] = {};

                    $scope.category[key].index = $scope.fixturesinfo.findIndex(getindex, value.team1_id);
                    console.log(value.group_id);
                    if (value.group_id != "0") {
                        $scope.category[key].selectedgroupindex = $scope.fixturesinfo[$scope.category[key].index].groups.findIndex((group) => {
                            return group.id == value.group_id;
                        });
                        //console.log('selected group index', $scope.category[key].selectedgroupindex);

                    }

                    //                    console.log(value.time);
                    value.time = new Date(value.time).toString();
                    value.time = new Date(value.time.replace(/GMT\++[a-zA-Z0-9\)\(\ ]*/g, 'GMT')).toLocaleTimeString('en-US', {
                        hour12: false
                    }).replace(/:\d+$/, '');
                    //  value.time="10:45";
                    value.time = new Date(new Date().toDateString() + ' ' + value.time);
                    // console.log(value, $scope.fixturesinfo[$scope.category[key].index]);
                });
            };

        };

        // FUNCTIONS

        /*SET TEAMS DATA INITIALLY FOR MATCHTYPES OTHER THAN GROUP*/
        var setteamsoncategorylevel = function () {
            _.forEach($scope.fixturesinfo, function (value, key) {
                $scope.teamofcategory[value.id] = [];
                for (var index in value.groups) {

                    $scope.teamofcategory[value.id] = $scope.teamofcategory[value.id].concat(value.groups[index].teams);
                }

            });
        };

        $scope.search = {
            category: ''
        };

        console.log($scope.search);

        // ADD A NEW FIXTURE WITH DUMMY OBJECT
        $scope.addnewfixture = function () {

            console.log(typeof $scope.search.category);

            //GET VALUES OF LAST FIXTURE INTO NEW FIXTURE OBJECT
            var lastFixture = $scope.fixtures[$scope.fixtures.length - 1];
            $scope.fixtures.push({
                'id': '',
                'date': lastFixture ? lastFixture.date : new Date(),
                /*lastFixture ? lastFixture.group_id : 0*/
                'group_id': "",
                /**/
                'label_1': "",
                'label_2': "",
                /*lastFixture ? lastFixture.matchtype_id : 1*/
                'matchtype_id': "",
                /**/
                'team1_id': "",
                'team2_id': "",
                'time': "",
                'tournament_id': tournamentid,
                'venue_id': lastFixture ? lastFixture.venue_id : 1,
                results: [{
                    penaltyscore: null,
                    result: 0,
                    score: 0,
                    team_id: 0
                }, {
                    penaltyscore: null,
                    result: 0,
                    score: 0,
                    team_id: 0
                }]
            });



            $scope.category.push({});
            $scope.category[$scope.fixtures.length - 1].index = parseInt($scope.search.category);

            //            $scope.category[$scope.fixtures.length - 1].selectedgroupindex = $scope.fixturesinfo[].groups.findIndex(checkforindex, $scope.fixtures[$scope.fixtures.length - 1].group_id);
            $scope.fixtures[$scope.fixtures.length - 1].results[0].team_id = $scope.fixtures[$scope.fixtures.length - 1].team1_id;
            $scope.fixtures[$scope.fixtures.length - 1].results[1].team_id = $scope.fixtures[$scope.fixtures.length - 1].team2_id;

            Object.defineProperty($scope.fixtures[$scope.fixtures.length - 1], 'id', {
                writable: true
            });
            //            console.log($scope.fixtures[$scope.fixtures.length - 1]);

            // SCROLL TO THE NEW ELEMENT
            $('.body-create-fixtures').scrollTop($('.body-create-fixtures')[0].scrollHeight + 300);

            //            console.log('#fixture-item-' + (($scope.fixtures.length) - 1));
            $('#fixture-item-' + (($scope.fixtures.length) - 1)).addClass('new-block');

        };

        // CANCEL CREATION OF A FIXTURE
        $scope.cancelfixture = function (ind) {
            //            console.log($scope.fixtures[ind]);
            if ($scope.fixtures[ind].id != '') {
                $filter('setpropertyfalse')($scope.fixtures[ind], 'id');
                for (var property in originaldata[ind]) {
                    $scope.fixtures[ind][property] = originaldata[ind][property];
                    //                    console.log(property + "-" + typeof $scope.fixtures[ind][property]);
                };
                $filter('setpropertyfalse')($scope.fixtures[ind].results[0], 'score');
            } else {
                $scope.fixtures.splice(ind, 1);
            }


        };


        // SUBMIT THE FIXTURE AFTER CREATE OR EDIT
        $scope.submitfixture = function (ind) {

            //            console.log($scope.fixtures[ind]);
            //            console.log($scope.fixtures[ind].id);

            if ($scope.fixtures[ind].id == "") {

                // CREATE A FIXTURE
                var createfixturesuccess = function (response) {

                    if (response.data != 'false') {
                        //                        console.log(response.data);
                        //$scope.fixtures[ind].id = response.data;
                        $scope.fixtures[ind].id = response.data;

                        $filter('setpropertyfalse')($scope.fixtures[ind], 'id');
                        $filter('setpropertyfalse')($scope.fixtures[ind].results[0], 'score');
                        //                        console.log($scope.fixtures[ind]);
                    };
                };
                var createfixtureerror = function (response) {
                    //                    console.log(response.data);
                };
                NavigationService.storepostdata('fixtures', $scope.fixtures[ind]).then(createfixturesuccess, createfixtureerror);

            } else {

                // EDIT A FIXTURE
                var editfixturesuccess = function (response) {
                    //                    console.log(response.data);
                    $filter('setpropertyfalse')($scope.fixtures[ind], 'id');
                };
                var editfixtureerror = function (response) {
                    //                    console.log(response.data);
                };
                NavigationService.editdata('fixtures', {
                    "id": $scope.fixtures[ind].id,
                    "data": angular.toJson($scope.fixtures[ind])
                }).then(editfixturesuccess, editfixtureerror);

            };
        };

        // MAKE FIXTURE EDITABLE
        $scope.editfixture = function (ind) {
            Object.defineProperty($scope.fixtures[ind], 'id', {
                writable: true
            });
            originaldata[ind] = angular.copy($scope.fixtures[ind]);
        };

        // DELETE A FIXTURE
        $scope.deletefixture = function (ind) {

            // SHOW CONFIRM POPUP

            var deletefixturebyidsuccess = function (response) {
                //                console.log(response.data);
                $scope.fixtures.splice(ind, 1);
            };
            var deletefixturebyiderror = function (response) {
                //                console.log(response.data);
                // SHOW ERROR - INTERNET ERROR
                NavigationService.showerror("Error connecting to the Internet");
            };
            NavigationService.deletefixturebyid($scope.fixtures[ind].id).then(deletefixturebyidsuccess, deletefixturebyiderror);
        };

        $scope.score = [];

        $scope.penaltyscore = [];

        // SUBMIT A RESULT
        $scope.submitresult = function (ind) {
            //            console.log($scope.fixtures[ind]);
            var result = {
                'fixture_id': $scope.fixtures[ind].id,
                'team1score': $scope.fixtures[ind].results[0].score,
                'team2score': $scope.fixtures[ind].results[1].score,
                'team1penalty': $scope.fixtures[ind].results[0].penaltyscore,
                'team2penalty': $scope.fixtures[ind].results[1].penaltyscore,
                'team1_id': $scope.fixtures[ind].team1_id,
                'team2_id': $scope.fixtures[ind].team2_id
            };

            //            console.log(result);

            var submitresultsuccess = function (response) {
                //                console.log(response.data);
                $filter('setpropertyfalse')($scope.fixtures[ind].results[0], 'score');
            };
            var submitresulterror = function (response) {
                //                console.log(response.data);
                // SHOW ERROR - INTERNET ERROR
                NavigationService.showerror("Error connecting to the Internet");
            };
            // SEND RESULT TO DATABASE, THIS API ALSO HANDLES EDIT RESULT
            NavigationService.submitresult(result).then(submitresultsuccess, submitresulterror);

        };

        // EDIT A RESULT
        $scope.editresult = function (ind) {
            //            console.log($scope.fixtures[ind].id);
            $filter('setproperty')($scope.fixtures[ind].results[0], 'score');
        };

        // CallBacks
        getdatabyidsuccess = function (response) {
            $scope.fixture = response.data
        };
        getdatabyiderror = function (error) {
            //            console.log(error);
        };

        matchtypessuccess = function (response) {
            //            console.log(response);
            $scope.matchtypes = response.data;
        }
        matchtypeserror = function (error) {
            //            console.log('Internet Error');
        }

        fixturechangesuccess = function (response) {
            if (response.data) {

            }
        };
        fixturechangeerror = function (error) {
            //            console.log('Internet Error');
        }
        getdataforfixturessuccess = function (response) {
            $scope.fixturesinfo = response.data;
            //            console.log($scope.fixturesinfo);
            addcategoryindex();
            setteamsoncategorylevel();
        }
        getdataforfixtureserror = function (error) {
            //            console.log('Internet Error');
        };

        getalldatasuccess = function (response) {
            $scope.fixtures = response.data;
            /*var result = {};
            $scope.fixtures['team-1-score']
            $scope.fixtures['team-2-score']
            $scope.fixtures['team-1-penalty']
            $scope.fixtures['team-2-penalty']*/

            addcategoryindex();

            console.log("FIXTURES", response.data);

            $scope.fixtures = _.map($scope.fixtures, function (element) {

                Object.defineProperty(element, 'id', {
                    writable: false
                });

                Object.defineProperty(element.results[0], 'score', {
                    writable: false
                });
                return element;
            });
        };
        getalldataerror = function (error) {
            //            console.log('Internet Error');
        };

        // Scope function
        $scope.savefixture = function () {
            //            console.log($scope.fixture);
            if (editid > 0) {
                NavigationService.editdata('fixtures', {
                    "id": $scope.fixture.id,
                    "data": angular.toJson($scope.fixture)
                }).then(fixturechangesuccess, fixturechangeerror);
            } else {
                NavigationService.storepostdata('fixtures', $scope.fixture).then(fixturechangesuccess, fixturechangeerror);
            }
        };

        $scope.selectedgroupindex = 0;



        $scope.getgroups = function (category) {
            // console.log(categoryindex);
            category.selectedgroupindex = 1;
            $scope.groups = $scope.fixturesinfo[category.index].groups;
        };

        $scope.getteams = function (fixtureindex, fixture) {
            //            console.log('get team called');
            $scope.teams = [];
            var categoryindex = $scope.category[fixtureindex].index;
            console.log(categoryindex);

            $scope.category[fixtureindex].selectedgroupindex = $scope.fixturesinfo[categoryindex].groups.findIndex((group) => {
                return group.id == fixture.group_id
            });
            console.log($scope.category[fixtureindex].selectedgroupindex);
            if (fixture.matchtype_id != 1) {
                //                console.log('else');
                var groups = $scope.fixturesinfo[categoryindex].groups;
                for (var index in groups) {
                    $scope.teams = $scope.teams.concat(groups[index].teams);
                };
                //                console.log($scope.teams);
            };
        };




        // CALL TO GET MATCH TYPES
        NavigationService.getalldata('matchtypes').then(matchtypessuccess, matchtypeserror);

        //CALL TO GET VENUES
        NavigationService.getalldata('venues').then(function (response) {
            $scope.venues = response.data;
        }, getalldataerror);

        //CALL TO GET FIXTURES DATA
        NavigationService.getfixturesbytournamentid(tournamentid).then(getalldatasuccess, getalldataerror);

        //GET INFO FOR FIXTURES DROPDOWN
        NavigationService.getdataforfixtures().then(getdataforfixturessuccess, getdataforfixtureserror);

  }
]);

phonecatControllers.controller('createteamCtrl', ['$scope', 'TemplateService', 'NavigationService', '$routeParams', '$location', 'FileUploader', '$http',
  function ($scope, TemplateService, NavigationService, $routeParams, $location, FileUploader, $http) {
        $scope.template = TemplateService;
        TemplateService.content = "views/createteam.html";
        $scope.title = "createteam";

        // Initializations
        var editid = $routeParams.editid;
        $scope.team = {};


        // Local functions
        var updatedata = function () {
            //            console.log('called');
            if (editid == 0) {
                NavigationService.storepostdata('teams', $scope.team).then(storeteamdatasuccess, storeteamdataerror);
            } else {
                NavigationService.editdata('teams', {
                    "id": $scope.team.id,
                    "data": angular.toJson($scope.team)
                }).then(storeteamdatasuccess, storeteamdataerror);
            }

        }

        // CallBacks
        storeteamdatasuccess = function (response) {
            //            console.log('Team registered successfully !', response);
            $location.path('teams');
        }
        storeteamdataerror = function (response) {
            //            console.log('Internet Error');
        }

        uploadimagesuccess = function (response) {
            //            console.log('upload image success');
            $scope.team.logo = response.data;
            updatedata();


        }
        uploadimageerror = function (response) {
            //            console.log('Internet Error');
        }
        getdatabyidsuccess = function (response) {
            //            console.log(response);
            $scope.team = response.data;
        }
        getdatabyiderror = function (error) {
            //            console.log('Internet Error');
        }

        // Scope functions
        $scope.registerteam = function () {
            //            console.log($scope.team);
            if ($scope.team.logo && $scope.team.logo.name) {
                //                console.log('in if');
                NavigationService.uploadimage({
                    "path": "teams",
                    "image": $scope.team.logo
                }).then(uploadimagesuccess, uploadimageerror);
            } else {
                //                console.log('in else');
                updatedata();
            }

        }

        if (editid > 0) {
            NavigationService.getdatabyid('teams', editid).then(getdatabyidsuccess, getdatabyiderror);
        }
        $scope.cancelaction = function () {
            $location.path('teams');
        }

        // Navigation services functions


  }
]);

/*Pointtablegenerator Controller*/

phonecatControllers.controller('pointtablegeneratorCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$http', '$rootScope', '$interval', 
  function ($scope, TemplateService, NavigationService, $location, $http, $rootScope, $interval) {
        $scope.template = TemplateService;
        TemplateService.content = "views/pointtablegenerator.html";
        $scope.title = "pointtable";
	}]);


phonecatControllers.controller('fixturesCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$http', '$rootScope', '$interval',
  function ($scope, TemplateService, NavigationService, $location, $http, $rootScope, $interval) {
        $scope.template = TemplateService;
        TemplateService.content = "views/fixtures.html";
        $scope.title = "fixtures";


        // Initializations
        $scope.fixtures = [];

        // callbacks
        getalldatasuccess = function (response) {
            $scope.fixtures = response.data;
        }
        getalldataerror = function (error) {
            //            console.log('Internet Error');
        }

        // SCOPE function
        $scope.deletefixture = function (fixture) {
            NavigationService.deletedatabyid('fixtures', fixture.id).then(function (response) {
                $scope.fixtures.splice($scope.fixtures.indexOf(fixture), 1);
            }, function (error) {
                //                console.log('Internet Error');
            });

        };


        $scope.gotocreatefixturepage = function (id) {
            $location.path('createfixtures/' + id);
        }

        // Navigation services
        // NavigationService.getalldata('fixtures').then(getalldatasuccess, getalldataerror);





  }
]);

phonecatControllers.controller('galleryCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$routeParams', 'textAngularManager', '$rootScope', '$interval', '$anchorScroll',
  function ($scope, TemplateService, NavigationService, $location, $routeParams, textAngularManager, $rootScope, $interval, $anchorScroll) {
        $scope.template = TemplateService;
        TemplateService.content = "views/gallery.html";
        $scope.title = "gallery";


        /*Initializations*/

        $scope.albums = [];

        $scope.photos = {};
        $scope.upload = {};
        $scope.photocollections = {};
        var initializeslider = function (albumid) {

            setTimeout(function () {
                //                console.log(albumid);
                $('#photoslider' + albumid).slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    nextArrow: '<div class="photos-next-arrow photos-slider-arrow"> <svg enable-background="new 0 0 451.846 451.847" version="1.1" viewBox="0 0 451.85 451.85" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path d="m345.44 248.29-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373z"/></svg> </div>',
                    prevArrow: '<div class="photos-prev-arrow photos-slider-arrow"> <svg enable-background="new 0 0 451.846 451.847" version="1.1" viewBox="0 0 451.85 451.85" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"><path d="m345.44 248.29-194.29 194.28c-12.359 12.365-32.397 12.365-44.75 0-12.354-12.354-12.354-32.391 0-44.744l171.91-171.91-171.91-171.9c-12.354-12.359-12.354-32.394 0-44.748 12.354-12.359 32.391-12.359 44.75 0l194.29 194.28c6.177 6.18 9.262 14.271 9.262 22.366 0 8.099-3.091 16.196-9.267 22.373z"/></svg> </div>',
                    infinite: false
                });
            }, 1000);
            /* ADD A PHOTO TO SLIDER */
            /* $('.photos-list').slick('slickAdd', '<li class="photo-item"><img src="https://www.thegamecrafter.com/overlays/largesquareboard.png" style="background-image:url()" /></li>');
              REMOVE A PHOTO FROM SLIDER 
             $('.photos-list').slick('slickRemove', '<li class="photo-item"><img src="https://www.thegamecrafter.com/overlays/largesquareboard.png" style="background-image:url(https://images.pexels.com/photos/46710/pexels-photo-46710.jpeg)" /></li>');*/

        }


        /*SCOPE FUNCTION*/
        $scope.createalbum = function () {
            $scope.albums.push({
                'name': ''
            })
        }


        /*Local Functions*/
        var getphotosbycategoryid = function (photo) {
            return photo.album_id == this;
        }
        getalbumssuccess = function (response) {
            $scope.albums = response.data;

            //            console.log('');
            NavigationService.getalldata('photos').then(function (response) {
                //                console.log(response);
                _.forEach($scope.albums, function (value, key) {

                    $scope.photos[value.id] = response.data.filter(getphotosbycategoryid, value.id);
                    $scope.photocollections[value.id] = angular.copy($scope.photos[value.id]);
                    initializeslider(value.id);
                });
                //                console.log($scope.photos);
            }, function (error) {

            });
            //            console.log(response);
        }
        getalbumserror = function (error) {
            //            console.log(error)
        }
        $scope.storealbum = function (album) {

            storepostdatasuccess = function (response) {
                album.id = response.data;
                initializeslider(album.id);
                //                console.log(response);
            }
            storepostdataerror = function (error) {
                //                console.log(error)
            }


            NavigationService.storepostdata('albums', album).then(storepostdatasuccess, storepostdataerror)
        }


        $scope.openfilechooser = function (index, album) {

            document.getElementById('fileInput' + index).click();
        }


        deletephoto = function (photoid, photoname, albumid) {

            deletedatabyidsuccess = function (response) {
                NavigationService.deleteimage({
                    path: 'gallery',
                    filename: photoname
                }).then(function (response) {
                    //                    console.log(response);
                }, function (error) {

                });
                //                console.log($scope.photocollections[albumid], photoid);
                var index = $scope.photocollections[albumid].findIndex(photo => photo.id == photoid);
                $scope.photocollections[albumid].splice(index, 1);
                //                console.log(index);
                $('#photoslider' + albumid).slick('slickRemove', index + 1);
                //                console.log(response);
            }
            deletedatabyiderror = function (error) {
                //                console.log(error)
            }

            //            console.log($('#photoslider' + albumid));
            NavigationService.deletedatabyid('photos', photoid).then(deletedatabyidsuccess, deletedatabyiderror);
        }
        $scope.deletealbum = function (album) {

            deletedatabyidsuccess = function (response) {
                $scope.albums.splice($scope.albums.indexOf(album), 1);

                //                console.log(response);
            }
            deletedatabyiderror = function (error) {
                //                console.log(error)
            }


            NavigationService.deletedatabyid('albums', album.id).then(deletedatabyidsuccess, deletedatabyiderror)
        };



        $scope.uploadimage = function (album) {
            //            console.log(typeof $scope.photos[album.id]);
            if ((typeof $scope.photos[album.id]) == "undefined") {

                $scope.photos[album.id] = [];
            }
            uploadimagesuccess = function (response) {
                //                console.log(response);
                var photo = {
                    name: response.data,
                    album_id: album.id
                };
                NavigationService.storepostdata('photos', photo).then(function (response) {
                    //                    console.log(response);
                    photo.id = response.data;



                    if (typeof $scope.photocollections[album.id] == "undefined") {
                        $scope.photocollections[album.id] = [];
                    }

                    $scope.photocollections[album.id].push(photo);



                    $('#photoslider' + album.id).slick('slickAdd', `<li class="photo-item" >
                    <div class="photo-actions" onclick="deletephoto(` + photo.id + `,'` + photo.name + `',` + album.id + `)">
                        <svg enable-background="new 0 0 774.266 774.266" version="1.1" viewBox="0 0 774.27 774.27" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                            <path d="m640.35 91.169h-103.38v-67.178c0-13.522-10.907-23.991-24.428-23.991-1.312 0-2.187 0.438-2.614 0.875-0.438-0.437-1.313-0.875-1.75-0.875h-246.46c-13.521 0-23.99 10.469-23.99 23.991v67.179h-103.82c-29.667 0-52.783 23.116-52.783 52.783v86.368h45.803v491.6c0 29.668 22.679 52.346 52.346 52.346h415.7c29.667 0 52.782-22.678 52.782-52.346v-491.6h45.366v-86.368c0-29.667-23.125-52.784-52.783-52.784zm-354.64-43.188h202.84v43.188h-202.84v-43.188zm313.64 673.94c0 3.061-1.312 4.363-4.364 4.363h-415.7c-3.052 0-4.364-1.303-4.364-4.363v-491.6h424.43v491.6zm45.366-539.58h-515.16v-38.387c0-3.053 1.312-4.802 4.364-4.802h506.44c3.053 0 4.365 1.749 4.365 4.802v38.387z" />
                            <rect x="475.03" y="286.59" width="48.418" height="396.94" />
                            <rect x="363.36" y="286.59" width="48.418" height="396.94" />
                            <rect x="251.69" y="286.59" width="48.418" height="396.94" />
                        </svg>
                        <p class="action-text">DELETE</p>
                    </div>
                    <img src="https://www.thegamecrafter.com/overlays/largesquareboard.png" style="background-image:url(` + imageurl + `gallery/` + photo.name + `)" />
                </li>`);


                    //                    console.log($scope.photocollections[album.id]);
                }, function (error) {

                });
                //                console.log(response);
            }
            uploadimageerror = function (error) {
                //                console.log(error)
            }


            NavigationService.uploadimage({
                path: "gallery",
                image: $scope.upload.name
            }).then(uploadimagesuccess, uploadimageerror)
        }






        NavigationService.getalldata('albums').then(getalbumssuccess, getalbumserror)
                    }

                    ]);

phonecatControllers.controller('groupsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$routeParams', 'textAngularManager', 'FileUploader', '$filter', '$rootScope', '$window', '$interval', '$anchorScroll',
  function ($scope, TemplateService, NavigationService, $location, $routeParams, textAngularManager, FileUploader, $filter, $rootScope, $window, $interval, $anchorScroll) {
        $scope.template = TemplateService;
        TemplateService.content = "views/groups.html";
        $scope.title = "groups";

        //      Initialization

        var categoryid = $routeParams.categoryid;

        $scope.groups = [];
        var originaldata = {};

        //      Callbacks
        getmanybysuccess = function (response) {
            $scope.groups = _.map(response.data, function (element) {
                Object.defineProperty(element, 'name', {
                    writable: false
                });
                return element;
            });
            //            console.log(response);
        }

        getmanybyerror = function (error) {
            //            console.log('Internet Error');
        }






        /*Scope functions*/
        $scope.deletegroup = function (group) {

            deletedatabyidsuccess = function (response) {
                //                console.log(response);
                $scope.groups.splice($scope.groups.indexOf(group), 1);
            }
            deletedatabyiderror = function (error) {
                //                console.log('Internet Error');
            }

            NavigationService.deletedatabyid('groups', group.id).then(deletedatabyidsuccess, deletedatabyiderror);
        }
        $scope.savegroup = function (group) {
            storepostdatasuccess = function (response) {
                //                console.log(response);
                if (!group.id) {
                    group.id = response.data;

                }
                Object.defineProperty(group, 'name', {
                    writable: false
                });
            }
            storepostdataerror = function (error) {
                //                console.log('Internet Error');
            }

            if (group.name) {
                if (group.id) {
                    NavigationService.editdata('groups', {
                        'id': group.id,
                        'data': angular.toJson(group)
                    }).then(storepostdatasuccess, storepostdataerror);
                } else {
                    group.category_id = categoryid;
                    NavigationService.storepostdata('groups', group).then(storepostdatasuccess, storepostdataerror);
                }
            }
        };

        $scope.canceladdition = function (group) {
            if (group.id) {
                group.name = originaldata[group.id].name;
                Object.defineProperty(group, 'name', {
                    writable: false
                });

            } else {
                var index = $scope.groups.indexOf(group);
                $scope.groups.splice(index, 1);
            }


        };

        $scope.editgroup = function (group) {
            originaldata[group.id] = angular.copy(group);
            Object.defineProperty(group, 'name', {
                writable: true
            });

        }


        //        $scope.gototeampage = function (groupid) {
        //            $location.path('teams/' + groupid);
        //        };

        /*Navigation functions*/
        NavigationService.getmanyby('groups', 'category_id', categoryid).then(getmanybysuccess, getmanybyerror);



  }
]);

phonecatControllers.controller('photouploadCtrl', ['$scope', 'TemplateService', 'NavigationService', '$routeParams', '$location',
  function ($scope, TemplateService, NavigationService, $routeParams, $location) {
        $scope.template = TemplateService;
        TemplateService.content = "views/photoupload.html";
        $scope.title = "photoupload";


  }
]);



phonecatControllers.controller('teamsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$location', '$routeParams',
  function ($scope, TemplateService, NavigationService, $location) {
        $scope.template = TemplateService;
        TemplateService.content = "views/teams.html";
        $scope.title = "teams";

        // Initializations

        $scope.teams = [];



        // callbacks
        getalldatasuccess = function (response) {
            $scope.teams = response.data;
        }

        getalldataerror = function (error) {
            //            console.log(error);
        };
        NavigationService.getalldata('teams').then(getalldatasuccess, getalldataerror);



        // SCOPE FUNCTIONS
        $scope.gotocreateteampage = function (id) {
            $location.path('createteam/' + id);

        }
        $scope.deleteteam = function (team) {
            NavigationService.deletedatabyid('teams', team.id).then(function (response) {
                $scope.teams.splice($scope.teams.indexOf(team), 1);
            }, function () {});
        }
}]);

phonecatControllers.controller('headerctrl', ['$scope', 'TemplateService', '$location', '$rootScope', 'NavigationService',
  function ($scope, TemplateService, $location, $rootScope, NavigationService) {

        $scope.template = TemplateService;
        $scope.logout = function () {
            $.jStorage.flush();
            $location.path('login');
        }


  }
]);

phonecatControllers.controller('notificationsCtrl', ['$scope', 'TemplateService', '$location', '$rootScope', 'NavigationService',
  function ($scope, TemplateService, $location, $rootScope, NavigationService) {


        $scope.template = TemplateService;
        TemplateService.content = "views/notifications.html";
        $scope.title = "Notifications";



  }
]);


phonecatControllers.controller('createNotificationCtrl', ['$scope', 'TemplateService', '$location', '$rootScope', 'NavigationService',
  function ($scope, TemplateService, $location, $rootScope, NavigationService) {


        $scope.template = TemplateService;
        TemplateService.content = "views/createNotification.html";
        $scope.title = "Create Notification";
        $scope.notification = {};


  }
]);

phonecatControllers.controller('registrationsCtrl', ['$scope', 'TemplateService', 'NavigationService', '$rootScope', '$filter', '$window', '$location', '$routeParams', '$http',
  function ($scope, TemplateService, NavigationService, $rootScope, $filter, $window, $location, $routeParams, $http) {

        $scope.template = TemplateService;
        TemplateService.content = "views/registrations.html";
        $scope.title = "Registrations";


        //GET ALL REGISTRATIONS
        var getregistrationssuccess = function (response, status) {
            console.log(response.data);
            $scope.registrations = response.data;
        };
        var getregistrationserror = function (data, status) {
            console.log(data);
        };
        var getregistrations = function () {
            return $http.get('http://www.pixoloproductions.com/fcb/fcbescolarest/index.php/fcbregistrations/getall');
        };
        getregistrations().then(getregistrationssuccess, getregistrationserror);

        $scope.filter = {
            center: ""
        };
        /* Functionalities */
        $scope.getexcel = function () {
            console.log($scope.filter);
            var exceldata = $filter('filter')($scope.registrations, $scope.filter);
            console.log(exceldata);
            alasql('SELECT * INTO XLSX("registrations.xlsx",{headers:true}) FROM ?', [exceldata]);
        };


  }]);

phonecatControllers.controller('addTeamCtrl', ['$scope', 'TemplateService', 'NavigationService', '$rootScope', '$filter', '$window', '$location', '$routeParams',
  function ($scope, TemplateService, NavigationService, $rootScope, $filter, $window, $location, $routeParams) {
        $scope.template = TemplateService;
        TemplateService.content = "views/addteam.html";
        $scope.title = "Add Team";


        /*Initialization*/
        var groupid = $routeParams.groupid;
        $scope.teamsnotincludedingroup = [];
        $scope.groupteams = [];
        $scope.groupname = $routeParams.groupname;

        /*Scope functions*/
        $scope.addteamtogroup = function (team) {



            NavigationService.storepostdata('team_group', {
                "team_id": team.id,
                "group_id": groupid,

            }).then(function (response) {
                $scope.teamsnotincludedingroup.splice($scope.teamsnotincludedingroup.indexOf(team), 1);
                $scope.groupteams.push({
                    transaction_id: response.data,
                    team_id: team.id,
                    "team_name": team.name
                });
                //                console.log();
            }, function (error) {
                //                console.log('Internet Error');
            })
        }

        $scope.removeteamfromgroup = function (team) {

            deletedatabyidsuccess = function (response) {
                $scope.groupteams.splice($scope.groupteams.indexOf(team), 1);
                delete team['transaction_id'];
                $scope.teamsnotincludedingroup.push({
                    id: team.team_id,
                    "name": team.team_name
                });
                //                console.log(response);
            }
            deletedatabyiderror = function (error) {
                //                console.log(error)
            }


            NavigationService.deletedatabyid('team_group', team.transaction_id).then(deletedatabyidsuccess, deletedatabyiderror);
        }


        $scope.goback = function () {
            window.history.go(-1);
        }
        /*Callbacks*/

        getteamssuccess = function (response) {
            $scope.teamsnotincludedingroup = response.data;
            //            console.log(response);
        }
        getteamserror = function (error) {
            //            console.log(error)
        }
        getgroupteamsuccess = function (response) {
            $scope.groupteams = response.data;
            //            console.log(response);
        }
        getgroupteamerror = function (error) {
            //            console.log(error)
        }


        /*Navigation services*/
        NavigationService.getteams().then(getteamssuccess, getteamserror);



        NavigationService.getgroupteam(groupid).then(getgroupteamsuccess, getgroupteamerror)


  }
]);
