-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 15, 2018 at 05:39 AM
-- Server version: 5.6.25-73.0-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pixolpvd_fcbescola`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `allfixtures`
-- (See below for the actual view)
--
CREATE TABLE `allfixtures` (
`id` int(11)
,`team-1_id` int(11)
,`team-2_id` int(11)
,`tournament_id` int(11)
,`group_id` int(11)
,`matchtype_id` int(11)
,`label_1` varchar(255)
,`label_2` varchar(255)
,`venue_id` int(11)
,`date` date
,`time` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `age` varchar(10) NOT NULL,
  `tournament_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `age`, `tournament_id`) VALUES
(1, 'boys', '14', 3);

-- --------------------------------------------------------

--
-- Table structure for table `fixtures`
--

CREATE TABLE `fixtures` (
  `id` int(11) NOT NULL,
  `team-1_id` int(11) NOT NULL,
  `team-2_id` int(11) NOT NULL,
  `tournament_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `matchtype_id` int(11) NOT NULL,
  `label_1` varchar(255) NOT NULL,
  `label_2` varchar(255) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fixtures`
--

INSERT INTO `fixtures` (`id`, `team-1_id`, `team-2_id`, `tournament_id`, `group_id`, `matchtype_id`, `label_1`, `label_2`, `venue_id`, `date`, `time`) VALUES
(1, 1, 2, 24, 1, 1, '', '', 1, '2018-05-02', '2018-05-14 20:13:09'),
(2, 1, 5, 24, 1, 1, '', '', 1, '2018-05-02', '2018-05-14 20:12:53'),
(3, 1, 6, 24, 1, 1, '', '', 1, '2018-05-02', '2018-05-14 20:13:31'),
(4, 2, 5, 24, 1, 1, '', '', 1, '2018-05-02', '2018-05-14 20:13:48'),
(5, 2, 6, 24, 1, 1, '', '', 1, '2018-05-03', '2018-05-14 20:14:25'),
(6, 5, 6, 24, 1, 1, '', '', 1, '2018-05-03', '2018-05-14 20:14:37');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `category_id`) VALUES
(1, 'Group A', 1);

-- --------------------------------------------------------

--
-- Table structure for table `matchtypes`
--

CREATE TABLE `matchtypes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matchtypes`
--

INSERT INTO `matchtypes` (`id`, `name`) VALUES
(1, 'group'),
(2, 'pre-quaters'),
(3, 'quater-final'),
(4, 'semi-final'),
(5, 'final'),
(6, 'third-place');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `album_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `fixture_id` int(11) NOT NULL,
  `team-1-score` int(11) NOT NULL,
  `team-2-score` int(11) NOT NULL,
  `team-1-penalty` int(11) DEFAULT NULL,
  `team-2-penalty` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `fixture_id`, `team-1-score`, `team-2-score`, `team-1-penalty`, `team-2-penalty`) VALUES
(1, 1, 3, 1, 0, 0),
(2, 2, 2, 2, NULL, NULL),
(3, 3, 2, 0, NULL, NULL),
(4, 4, 2, 3, NULL, NULL),
(5, 5, 2, 4, NULL, NULL),
(6, 6, 4, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE `stories` (
  `id` int(11) NOT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `image` text,
  `video` text,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `logo` text NOT NULL,
  `initials` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `logo`, `initials`, `parent_id`) VALUES
(1, 'Skorost United F.C.', 'fcbescola1526362424argo_image.png', 'sk', NULL),
(2, 'Skorost', 'fcbescola1526362407banner.jpg', 'SK', NULL),
(5, 'FC Barcelona', 'fcbescola152630541315_trends.jpg', 'FCB', NULL),
(6, 'Chelsea', 'fcbescola1526305444activate.png', 'che', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team_group`
--

CREATE TABLE `team_group` (
  `id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_group`
--

INSERT INTO `team_group` (`id`, `team_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 5, 1),
(4, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tournaments`
--

CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `primary_color` varchar(255) NOT NULL,
  `secondary_color` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tournaments`
--

INSERT INTO `tournaments` (`id`, `name`, `primary_color`, `secondary_color`) VALUES
(25, 'world cups', '#aabbcc', '#eeddaa'),
(24, 'champions league', '#767667', '#765434');

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE `venues` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `name`) VALUES
(1, 'Venue 1'),
(2, 'Venue 2');

-- --------------------------------------------------------

--
-- Structure for view `allfixtures`
--
DROP TABLE IF EXISTS `allfixtures`;

CREATE ALGORITHM=UNDEFINED DEFINER=`pixolpvd`@`localhost` SQL SECURITY DEFINER VIEW `allfixtures`  AS  select `fixtures`.`id` AS `id`,`fixtures`.`team-1_id` AS `team-1_id`,`fixtures`.`team-2_id` AS `team-2_id`,`fixtures`.`tournament_id` AS `tournament_id`,`fixtures`.`group_id` AS `group_id`,`fixtures`.`matchtype_id` AS `matchtype_id`,`fixtures`.`label_1` AS `label_1`,`fixtures`.`label_2` AS `label_2`,`fixtures`.`venue_id` AS `venue_id`,`fixtures`.`date` AS `date`,`fixtures`.`time` AS `time` from `fixtures` where 1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fixtures`
--
ALTER TABLE `fixtures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matchtypes`
--
ALTER TABLE `matchtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stories`
--
ALTER TABLE `stories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_group`
--
ALTER TABLE `team_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tournaments`
--
ALTER TABLE `tournaments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `fixtures`
--
ALTER TABLE `fixtures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `matchtypes`
--
ALTER TABLE `matchtypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `stories`
--
ALTER TABLE `stories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `team_group`
--
ALTER TABLE `team_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tournaments`
--
ALTER TABLE `tournaments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
